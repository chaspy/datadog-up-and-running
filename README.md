# Datadog Up and Running

## How to collect metrics

```mermaid
graph LR
  subgraph Source
    Host
    subgraph Host
      DataDogAgent
    end
  end
  DataDogAgent --SendMetrics--> DataDog
  Host--SendMetrics-->Aggregator
  Aggregator--KickDataDogAPI-->DataDog
```

## DataDog words relation

```mermaid
graph LR
  Source--Event-->DataDog
  Source--Metrics-->DataDog
  Metrics--CollectByEachHost-->Infrastructure
  Metrics-->Monitor
  Monitor-->Slack
  Metrics--ViewByOurConditions-->Dashboard